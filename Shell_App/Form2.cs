﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell_App
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Veuillez remplir tous les Champs Obligatoires");
                return;
            }

            try
            {
                cla.con.Open();
                cla.cmd.CommandText = ("select count(*),role from users where username= '" + textBox1.Text + "' and password= '" + textBox2.Text + "'" + "Group by role");
                cla.rdr = cla.cmd.ExecuteReader();
                DataTable dtt = new DataTable();
                dtt.Load(cla.rdr);
                if (dtt.Rows.Count > 0)
                {
                    this.Hide();
                    Form1 fr = new Form1(dtt.Rows[0][1].ToString());
                    fr.ShowDialog();
                }
                else
                {
                    throw new Exception("Nom d'utilisateur ou Mot de passe est Incorrect");
                }
            
             }
            catch (Exception ex)
            { 
               MessageBox.Show(ex.Message);
            }
            finally
            {
                if (cla.con.State == ConnectionState.Open)
                {
                    cla.con.Close();
                }
            }
            
          }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
