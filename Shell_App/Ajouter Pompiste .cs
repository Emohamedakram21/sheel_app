﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell_App
{
    public partial class Ajouter_Pompiste : Form
    {
        public Ajouter_Pompiste()
        {
            InitializeComponent();
        }

        DataClasses1DataContext db = new DataClasses1DataContext();

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == " ")
            {
                MessageBox.Show("Une information est vide");
                return;
            }

            try
            {
                pompiste p = new pompiste();
                p.CIN = textBox1.Text;
                p.nom = textBox2.Text;
                p.prenom = textBox3.Text;
                p.adresse = textBox4.Text;
                p.tel = textBox5.Text;
                p.dateEntree = dateTimePicker1.Value;
                db.pompiste.InsertOnSubmit(p);
                db.SubmitChanges();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
