﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell_App
{
    public partial class Pompistes : Form
    {
        public Pompistes()
        {
            InitializeComponent();
        }

        public void remplissageData()
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            cla.con.Open();
            cla.cmd.CommandText = "select id,cin,nom,prenom,tel,adresse,convert(varchar(10),dateEntree) as dateEntree from pompiste";
            cla.rdr = cla.cmd.ExecuteReader();
            while (cla.rdr.Read())
            {
                dataGridView1.Rows.Add(cla.rdr[0], cla.rdr[1], cla.rdr[2], cla.rdr[3], cla.rdr[4], cla.rdr[5], cla.rdr[6]);
            }
            cla.con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Ajouter_Pompiste pa = new Ajouter_Pompiste();
            pa.ShowDialog();
            dataGridView1.Rows.Clear();
            remplissageData();
        }

        private void Pompistes_Load(object sender, EventArgs e)
        {
            dataGridView1.Columns.Add("id", "N°");
            dataGridView1.Columns.Add("CIN", "CIN");
            dataGridView1.Columns.Add("nom", "Nom");
            dataGridView1.Columns.Add("prenom", "Prénom");
            dataGridView1.Columns.Add("tel", "Tél");
            dataGridView1.Columns.Add("adresse", "Adresse");
            dataGridView1.Columns.Add("dateEntree", "Date Entrée");

            remplissageData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            try
            {
                DialogResult res = MessageBox.Show("Voulez_Vous vraiment Suprimmer cette ligne", "CONFIRAMTION", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    cla.con.Open();
                    cla.cmd.CommandText = "Delete from pompiste where id =" + dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    cla.cmd.ExecuteNonQuery();
                    cla.con.Close();
                    dataGridView1.Rows.Clear();
                    remplissageData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            try
            {
                cla.con.Open();
                cla.cmd.CommandText = "Update pompiste set CIN = '" +
                    dataGridView1.CurrentRow.Cells[1].Value.ToString() + "',nom = '" +
                    dataGridView1.CurrentRow.Cells[2].Value.ToString() + "',prenom ='" +
                    dataGridView1.CurrentRow.Cells[3].Value.ToString() + "',Tel ='" +
                    dataGridView1.CurrentRow.Cells[5].Value.ToString() + "',adresse ='" +
                    dataGridView1.CurrentRow.Cells[4].Value.ToString() + "' where id = " +
                    dataGridView1.CurrentRow.Cells[0].Value.ToString();
                cla.cmd.ExecuteNonQuery();
                MessageBox.Show("La mise à jour du pompiste a été effectuée avec succés  ");
                cla.con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
