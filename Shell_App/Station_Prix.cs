﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Shell_App
{
    public partial class Station_Prix : Form
    {
        public Station_Prix()
        {
            InitializeComponent();
        }

        public void remplissageData()
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            cla.con.Open();
            cla.cmd.CommandText = "Select * from carburant";
            cla.rdr = cla.cmd.ExecuteReader();
            while (cla.rdr.Read())
            {
                dataGridView1.Rows.Add(cla.rdr[0],cla.rdr[1], cla.rdr[2]);
            }
            cla.con.Close();
        }

        private void Station_Prix_Load(object sender, EventArgs e)
        {
            dataGridView1.Columns.Add("id", "ID");
            dataGridView1.Columns.Add("libelle", "Type");
            dataGridView1.Columns.Add("prix", "Prix");
            remplissageData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            try
            {
                 cla.con.Open();
                 cla.cmd.CommandText = "update carburant set prix =" + (dataGridView1.CurrentRow.Cells[2].Value.ToString()).Replace(",", ".") + "where id =" + dataGridView1.CurrentRow.Cells[0].Value.ToString();
                 cla.cmd.ExecuteNonQuery();
                 MessageBox.Show("Modifier");
                 cla.con.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
