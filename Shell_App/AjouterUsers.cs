﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell_App
{
    public partial class AjouterUsers : Form
    {
        public AjouterUsers()
        {
            InitializeComponent();
        }

        DataClasses1DataContext db = new DataClasses1DataContext();

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox2.Text != textBox3.Text)
            {
                MessageBox.Show("Mot de Passe Incorrect");
                textBox2.Text = "";
                textBox3.Text = "";
                return;
            }

            if(textBox1.Text == "" ||textBox2.Text == "" || textBox3.Text == "" || comboBox1.Text == "" )
            {
                MessageBox.Show("Veuillez remplir tous les Champs Obligatoires");
                return;
            }

            try
            {
                users u = new users();
                u.username = textBox1.Text;
                u.password = textBox2.Text;
                u.role = comboBox1.Text;
                db.users.InsertOnSubmit(u);
                db.SubmitChanges();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
