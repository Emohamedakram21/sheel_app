﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell_App
{
    public partial class ListUsers : Form
    {
        public ListUsers()
        {
            InitializeComponent();
        }

        public void remplissageData()
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            cla.con.Open();
            cla.cmd.CommandText = "Select id,username,role from users";
            cla.rdr = cla.cmd.ExecuteReader();
            while (cla.rdr.Read())
            {
                dataGridView1.Rows.Add(cla.rdr[0], cla.rdr[1], cla.rdr[2]);
            }
            cla.con.Close();
        }

        private void ListUsers_Load(object sender, EventArgs e)
        {
            dataGridView1.Columns.Add("id", "N°");
            dataGridView1.Columns.Add(" username", " Nom Utilisateur");
            dataGridView1.Columns.Add("role", "Role");

             remplissageData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AjouterUsers a = new AjouterUsers();
            a.ShowDialog();
            dataGridView1.Rows.Clear();
            remplissageData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            try
            {
                DialogResult res = MessageBox.Show("Voulez_Vous vraiment Suprimmer cette ligne", "CONFIRAMTION", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    cla.con.Open();
                    cla.cmd.CommandText = "Delete from users where id =" + dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    cla.cmd.ExecuteNonQuery();
                    cla.con.Close();
                    dataGridView1.Rows.Clear();
                    remplissageData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            try
            {
                cla.con.Open();
                cla.cmd.CommandText = "Update users set username = '" +
                    dataGridView1.CurrentRow.Cells[1].Value.ToString() + "',role = '" +
                    dataGridView1.CurrentRow.Cells[2].Value.ToString() + "' where id = " +
                    dataGridView1.CurrentRow.Cells[0].Value.ToString();
                cla.cmd.ExecuteNonQuery();
                MessageBox.Show("La mise à jour du client a été effectuée avec succés  ");
                cla.con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
