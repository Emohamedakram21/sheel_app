﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell_App
{
    public partial class Form1 : Form
    {
        public string role;

        public Form1(string reslt)
        {
            role = reslt;
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void ajouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ajouter_Pompiste pa = new Ajouter_Pompiste();
            pa.ShowDialog();
        }

        private void stationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pompisteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pompistes p = new Pompistes();
            p.ShowDialog();
        }

        private void modifierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Station_Prix s = new Station_Prix();
            s.ShowDialog();
            gasoil();
            essence();
            v_power();
        }

        private void nouveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjouterClient cl = new AjouterClient();
            cl.ShowDialog();
        }

        private void ajouterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Clients c = new Clients();
            c.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox3.Text = (Convert.ToInt32(textBox1.Text) - Convert.ToInt32(textBox2.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }



        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                 double gasoil = double.Parse(label15.Text, CultureInfo.InvariantCulture.NumberFormat) ;
                 double essence = double.Parse(label18.Text, CultureInfo.InvariantCulture.NumberFormat);
                 double v_power = double.Parse(label20.Text, CultureInfo.InvariantCulture.NumberFormat);

                textBox25.Text = (essence * Convert.ToDouble(textBox3.Text)).ToString();
                textBox26.Text = (essence * Convert.ToDouble(textBox4.Text)).ToString();
                //Gasoil
                textBox27.Text = (gasoil * Convert.ToDouble(textBox7.Text)).ToString();
                textBox28.Text = (gasoil * Convert.ToDouble(textBox10.Text)).ToString();
                textBox31.Text = (gasoil * Convert.ToDouble(textBox13.Text)).ToString();
                textBox32.Text = (gasoil * Convert.ToDouble(textBox16.Text)).ToString();
                //V-power
                textBox29.Text = (v_power * Convert.ToDouble(textBox19.Text)).ToString();
                textBox30.Text = (v_power * Convert.ToDouble(textBox22.Text)).ToString();
                //
                textBox33.Text = "CAFE";
                textBox34.Text = "LHUILE";
                textBox35.Text = "LAVAGE";

            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            gasoil();
            essence();
            v_power();
            label2.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");

            if (role == "membre")
            {
                pompisteToolStripMenuItem.Visible = false;
                //button4.Visible = false;
            }
        }

        public void gasoil()
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            cla.con.Open();
            cla.cmd.CommandText = "select prix from carburant  where id = 1";
            label15.Text = cla.cmd.ExecuteScalar().ToString();
            cla.con.Close();
        }

        public void essence()
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            cla.con.Open();
            cla.cmd.CommandText = "select prix from carburant  where id = 2";
            label18.Text = cla.cmd.ExecuteScalar().ToString();
            cla.con.Close();
        }

        public void v_power()
        {
            if (cla.con.State == ConnectionState.Open)
            {
                cla.con.Close();
            }
            cla.con.Open();
            cla.cmd.CommandText = "select prix from carburant  where id = 3";
            label20.Text = cla.cmd.ExecuteScalar().ToString();
            cla.con.Close();
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox4.Text = (Convert.ToInt32(textBox6.Text) - Convert.ToInt32(textBox5.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox7.Text = (Convert.ToInt32(textBox9.Text) - Convert.ToInt32(textBox8.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox10.Text = (Convert.ToInt32(textBox12.Text) - Convert.ToInt32(textBox11.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox13.Text = (Convert.ToInt32(textBox15.Text) - Convert.ToInt32(textBox14.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }

        private void textBox18_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox16.Text = (Convert.ToInt32(textBox18.Text) - Convert.ToInt32(textBox17.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox19.Text = (Convert.ToInt32(textBox21.Text) - Convert.ToInt32(textBox20.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }

        private void textBox24_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox22.Text = (Convert.ToInt32(textBox24.Text) - Convert.ToInt32(textBox23.Text)).ToString();
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }
        DataClasses1DataContext db;
        private void comboBox1_MouseDown(object sender, MouseEventArgs e)
        {
            db = new DataClasses1DataContext();
            comboBox1.DataSource = (from p in db.pompiste select p.nom).ToList();
        }

        private void comboBox2_MouseDown(object sender, MouseEventArgs e)
        {
            db = new DataClasses1DataContext();
            comboBox2.DataSource = (from p in db.client select p.nom).ToList();
        }

        private void comboBox3_MouseDown(object sender, MouseEventArgs e)
        {
            db = new DataClasses1DataContext();
            comboBox3.DataSource = (from p in db.pompiste select p.nom).ToList();
        }

        private void nouveauToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AjouterUsers au = new AjouterUsers();
            au.ShowDialog();
        }

        private void listeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ListUsers u = new ListUsers();
            u.ShowDialog();
        }
    }
}


