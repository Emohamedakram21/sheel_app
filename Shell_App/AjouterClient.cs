﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell_App
{
    public partial class AjouterClient : Form
    {
        public AjouterClient()
        {
            InitializeComponent();
        }

        DataClasses1DataContext db = new DataClasses1DataContext();

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == " ")
            {
                MessageBox.Show("Une information est vide");
                return;
            }

            try
            {
                client c = new client();
                c.nom = textBox2.Text;
                c.prenom = textBox3.Text;
                c.tele = textBox4.Text;
                c.CIE = textBox5.Text;
                c.adresse = textBox1.Text;
                db.client.InsertOnSubmit(c);
                db.SubmitChanges();
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
